// Обратите внимание, что модуль не умеет обсчитывать полигоны с самопересечениями.
ymaps.ready(['util.calculateArea']).then(function () {
    var myMap = new ymaps.Map("map", {
            center: [55.741448, 49.184774],
            zoom: 16,
            controls: ['searchControl', 'zoomControl']
        }, {
            searchControlProvider: 'yandex#search'
        }),
        // Создаем многоугольник, круг и прямоугольник.
        polygon = new ymaps.GeoObject({
            geometry: {
                type: "Polygon", coordinates: [[
                    [55.745221, 49.190444],
                    [55.741666, 49.191393],
                    [55.741206, 49.190449],
                    [55.740172, 49.177852],
                    [55.743471, 49.177433],
                    [55.743791, 49.177734],
                    [55.743997, 49.178238],
                    [55.744988, 49.189821],
                    [55.745221, 49.190444]
                ]]
            }
        }),
        collection = new ymaps.GeoObjectCollection();
    // Добавляем геообъекты в коллекцию.
    collection.add(polygon);
    // Добавляем коллекцию на карту.
    myMap.geoObjects.add(collection);

    collection.each(function (obj) {
        // Вычисляем площадь геообъекта.
        var area = Math.round(ymaps.util.calculateArea(obj)),
            // Вычисляем центр для добавления метки.
            center = ymaps.util.bounds.getCenter(obj.geometry.getBounds());
        // Если площадь превышает 1 000 000 м², то приводим ее к км².
        if (area <= 1e6) {
            area += ' м²';
        } else {
            area = (area / 1e6).toFixed(3) + ' км²';
        }
        obj.properties.set('balloonContent', area);

        myMap.geoObjects.add(new ymaps.Placemark(center, {'iconCaption': area}, {preset: 'islands#greenDotIconWithCaption'}));
    });
});
