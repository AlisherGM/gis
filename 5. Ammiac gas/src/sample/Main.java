package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

import java.io.FileInputStream;

public class Main extends Application {
    double pi = 3.14;

    double angle;
    double speed;
    double time;
    double mass;

    Ellipse ellipse = new Ellipse();


    @Override
    public void start(Stage primaryStage) throws Exception {
        Group root = new Group();

        Image image = new Image(new FileInputStream("src/resources/map.png"));
        ImageView imageView = new ImageView(image);
        root.getChildren().add(imageView);

        double labelLayoutX = image.getWidth() + 10;

        Label l1 = new Label("Угол ветра(градус)");
        l1.setLayoutX(labelLayoutX);
        l1.setLayoutY(10);
        TextField angleTF = new TextField("45");
        angleTF.setLayoutX(labelLayoutX);
        angleTF.setLayoutY(30);

        Label l2 = new Label("Скорость ветра(км/ч");
        l2.setLayoutX(labelLayoutX);
        l2.setLayoutY(70);
        TextField speedTF = new TextField("10");
        speedTF.setLayoutX(labelLayoutX);
        speedTF.setLayoutY(90);

        Label l3 = new Label("Время расп-ния ветра(градус)");
        l3.setLayoutX(labelLayoutX);
        l3.setLayoutY(130);
        TextField timeTF = new TextField("15");
        timeTF.setLayoutX(labelLayoutX);
        timeTF.setLayoutY(150);

        Label l4 = new Label("Масса Аммиака(кг)");
        l4.setLayoutX(labelLayoutX);
        l4.setLayoutY(190);
        TextField massTF = new TextField("1200");
        massTF.setLayoutX(labelLayoutX);
        massTF.setLayoutY(210);


        double xPoint = image.getWidth() / 2 + 200;
        double yPoint = image.getHeight() / 2 + 200;
        double area = 0.42;//км^2
        double dir1 = 30;
        double dir2 = area / (pi * dir1);

        Line xLine = new Line(xPoint, yPoint, xPoint, yPoint-400);
        Line yLine = new Line(xPoint, yPoint, xPoint-400, yPoint);
        root.getChildren().addAll(xLine, yLine);



        Button button = new Button();
        button.setText("Вычислить");
        button.setLayoutX(labelLayoutX);
        button.setLayoutY(250);
        button.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                root.getChildren().remove(ellipse);
                angle = Double.parseDouble(angleTF.getText());
                speed = Double.parseDouble(speedTF.getText());
                time = Double.parseDouble(timeTF.getText());
                mass = Double.parseDouble(massTF.getText());

                double dir1 = speed*time;
                double dir2 = 50 + area / (pi * dir1);

                double ellipseX = xPoint + Math.cos(3.14 * (90 + angle) / 180) * (100 + dir1);
                double ellipseY = yPoint - Math.sin(3.14 * (90 + angle) / 180) * (100 + dir1);

                ellipse.setCenterX(ellipseX);
                ellipse.setCenterY(ellipseY);
                ellipse.setRadiusX(dir1);
                ellipse.setRadiusY(dir2);
                ellipse.setRotate(90-angle);
                ellipse.setStyle("-fx-fill: transparent; -fx-stroke: green");
                root.getChildren().add(ellipse);
            }
        });

        root.getChildren().addAll(l1, l2, l3, l4, angleTF, speedTF, timeTF, massTF, button);


        primaryStage.setMinHeight(image.getHeight());
        primaryStage.setMinWidth(image.getWidth() + 200);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
