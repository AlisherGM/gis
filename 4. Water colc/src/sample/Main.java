package sample;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Orientation;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ScrollBar;
import javafx.scene.image.*;
import javafx.scene.image.Image;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

import javax.sql.rowset.Predicate;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.ArrayList;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        Image secondStageImage = new Image(new FileInputStream("src/resources/second1.png"));
        ImageView secondStageImageView = new ImageView(secondStageImage);

        Image image = new Image(new FileInputStream("src/resources/first.png"));
        ImageView imageView = new ImageView(image);

        double height = image.getHeight();
        double width = image.getWidth();

        Group root = new Group();
        Scene scene = new Scene(root, width, height);


        ScrollBar scrollBar = new ScrollBar();
        scrollBar.setOrientation(Orientation.VERTICAL);
        scrollBar.setPrefHeight(365);
        scrollBar.setLayoutX(1);
        scrollBar.setLayoutY(130);

        scrollBar.setMin(1);
        scrollBar.setMax(360);
        scrollBar.setValue(500);

        ArrayList<Line> lines = new ArrayList<>();
        scrollBar.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                lines.forEach(line -> {
                    root.getChildren().remove(line);
                });
                int level = newValue.intValue();
                double startX = 0, endX = 750;
                Line line;
                for (double y = 140 + level; y < 495; y++) {//495-130/335=360
                    line = new Line(startX, y, endX, y);
                    line.setStyle("-fx-stroke: aqua");
                    lines.add(line);
                    root.getChildren().add(0, line);
                }
                try {
                    root.getChildren().remove(secondStageImageView);
                    if (level > 238) {
                        secondStageImageView.setImage(new Image(new FileInputStream("src/resources/second1.png")));
                    }else if (level < 238 && level > 136) {
                        secondStageImageView.setImage(new Image(new FileInputStream("src/resources/second2.png")));
                    }else if (level < 136 && level > 15) {
                        secondStageImageView.setImage(new Image(new FileInputStream("src/resources/second3.png")));
                    }else if (level < 15 && level > 3) {
                        secondStageImageView.setImage(new Image(new FileInputStream("src/resources/second4.png")));
                    }else{
                        secondStageImageView.setImage(new Image(new FileInputStream("src/resources/second5.png")));
                    }
                    root.getChildren().add(secondStageImageView);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
        imageView.setLayoutX(0);
        imageView.setFitHeight(500);
        imageView.setFitWidth(750);
        secondStageImageView.setLayoutX(780);
        secondStageImageView.setLayoutY(30);
        secondStageImageView.setFitHeight(500);
        secondStageImageView.setFitWidth(500);
        root.getChildren().addAll(imageView, secondStageImageView);
        root.getChildren().add(scrollBar);
        primaryStage.setScene(scene);
        primaryStage.toFront();
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
