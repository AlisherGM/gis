package sample;

import com.sun.prism.paint.Color;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.io.FileInputStream;


public class Main extends Application {
    double x1, y1, x2, y2, length;
    int mouseCount = 1;
    int mc = 1;
    Label label = new Label();
    Group root = new Group();
    Scene scene = new Scene(root, 941,791);
    Circle firstPoint = new Circle();
    Circle secondPoint = new Circle();
    Line line = new Line();

    double rectStartX, rectStartY, rectEndX, rectEndY;
    Line line1 = new Line();
    Line line2 = new Line();
    Line line3 = new Line();
    Line line4 = new Line();

    @Override
    public void start(Stage primaryStage) throws Exception{
        line.setFill(javafx.scene.paint.Color.GREEN);
        firstPoint.setRadius(3);
        firstPoint.setFill(javafx.scene.paint.Color.BLUE);
        secondPoint.setRadius(3);
        secondPoint.setFill(javafx.scene.paint.Color.RED);
        Image image = new Image(new FileInputStream("src/resources/map.jpg"));
        ImageView imageView = new ImageView(image);

        Button lengthButton = new Button("Длина");
        lengthButton.setLayoutX(10);
        lengthButton.setLayoutY(680);
        lengthButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent mouseEvent) {
                scene.addEventFilter(MouseEvent.MOUSE_PRESSED, (EventHandler<MouseEvent>) event -> {
                    if(mouseCount == 1){
                        x1 = event.getX();
                        y1 = event.getY();
                        firstPoint.setCenterX(x1);
                        firstPoint.setCenterY(y1);
                        line.setStartX(x1);
                        line.setStartY(y1);
                        length = Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
                        root.getChildren().remove(firstPoint);
                        root.getChildren().add(firstPoint);
                        mouseCount++;
                    }else{
                        x2 = event.getX();
                        y2 = event.getY();
                        secondPoint.setCenterX(x2);
                        secondPoint.setCenterY(y2);
                        line.setEndX(x2);
                        line.setEndY(y2);
                        line.getBoundsInLocal();
                        length = Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
                        root.getChildren().removeAll(secondPoint,line, label);
                        root.getChildren().addAll(secondPoint,line,label);
                        mouseCount--;
                    }

                    label.setText(String.valueOf(length) + " м");
                    System.out.println(Math.abs(x2-x1));
                    System.out.println(Math.abs(y2-y1));
                    label.setLayoutX(x2 + 10);
                    label.setLayoutY(y2 + 10);
                });
            }


        });


        Button areaButton = new Button("Площадь");
        areaButton.setLayoutX(85);
        areaButton.setLayoutY(680);
        areaButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent mouserEvent) {
                scene.addEventHandler(MouseEvent.ANY, (EventHandler<MouseEvent>) event -> {
                    if(event.getEventType().equals(MouseEvent.MOUSE_PRESSED)){
                        rectStartX = event.getX();
                        rectStartY = event.getY();
                    }
                    if(event.getEventType().equals(MouseEvent.MOUSE_DRAGGED)){
                        rectEndX = event.getX();
                        rectEndY = event.getY();
                        root.getChildren().removeAll(line1, line2, line3, line4);

                        line1.setStartX(rectStartX);
                        line1.setStartY(rectStartY);
                        line1.setEndX(rectStartX);
                        line1.setEndY(rectEndY);

                        line2.setStartX(rectStartX);
                        line2.setStartY(rectEndY);
                        line2.setEndX(rectEndX);
                        line2.setEndY(rectEndY);

                        line3.setStartX(rectEndX);
                        line3.setStartY(rectEndY);
                        line3.setEndX(rectEndX);
                        line3.setEndY(rectStartY);

                        line4.setStartX(rectStartX);
                        line4.setStartY(rectStartY);
                        line4.setEndX(rectEndX);
                        line4.setEndY(rectStartY);
                        root.getChildren().addAll(line1, line2, line3, line4);

                    }
                    if(event.getEventType().equals(MouseEvent.MOUSE_RELEASED)){
                        double area = Math.abs(rectEndX - rectStartX)*Math.abs(rectEndY-rectStartY)*1407/1200;
                        System.out.println(area);
                        System.out.println(rectStartX + ", " + rectStartY);
                        System.out.println(rectEndX + ", " + rectEndY);

                        label.setVisible(true);
                        label.setText(String.valueOf(area) + " м^2");
                        label.setLayoutX(10);
                        label.setLayoutY(10);
                    }
                });
            }
        });
        root.getChildren().addAll(imageView, lengthButton, areaButton);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }

    public static void areaButtonActionEvent(ActionEvent mouseEvent){

    }
}
